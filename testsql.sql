-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2021 at 12:16 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testsql`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `calculoPromedioFemenino` (IN `fechaInicio` DATE, IN `fechaFinal` DATE)  NO SQL
select round(sum(resultados.puntaje)/(SELECT COUNT(id_resultado) FROM resultados INNER JOIN usuarios on usuarios.id_usuario=resultados.id_usuario where sexo='Femenino')) as promedioF from resultados INNER JOIN usuarios on usuarios.id_usuario=resultados.id_usuario where sexo = 'Femenino' AND fecha >= fechaInicio AND fecha <= fechaFinal$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `calculoPromedioMasculino` (IN `fechaInicio` DATE, IN `fechaFinal` DATE)  NO SQL
select round(sum(resultados.puntaje)/(SELECT COUNT(id_resultado) FROM resultados INNER JOIN usuarios on usuarios.id_usuario=resultados.id_usuario where sexo='Masculino')) as promedioM from resultados INNER JOIN usuarios on usuarios.id_usuario=resultados.id_usuario where sexo = 'Masculino' AND fecha >= fechaInicio AND fecha <= fechaFinal$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `resultados`
--

CREATE TABLE `resultados` (
  `id_resultado` int(11) NOT NULL,
  `puntaje` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resultados`
--

INSERT INTO `resultados` (`id_resultado`, `puntaje`, `id_usuario`, `fecha`) VALUES
(1, 10, 1, '2021-02-27'),
(2, 9, 2, '2021-02-25'),
(3, 10, 3, '2021-02-21'),
(4, 9, 4, '2021-02-21'),
(11, 5, 2, '2021-02-22'),
(12, 5, 2, '2021-02-22'),
(16, 0, 5, '2021-02-22'),
(17, 1, 5, '2021-02-22'),
(18, 3, 5, '2021-02-22');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `contrasenia` longblob NOT NULL,
  `edad` int(11) NOT NULL,
  `sexo` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `correo`, `contrasenia`, `edad`, `sexo`) VALUES
(1, 'alfredo', 'sorrow2287@gmail.com', 0xd4004fdf0c8b25ba4f5156662f6244c5, 19, 'Masculino'),
(2, 'Danielita-chan', 'danielaAmoNaruto@gmail.com', 0xd4004fdf0c8b25ba4f5156662f6244c5, 19, 'Femenino'),
(3, 'Hombre', 'dagfgdf', 0xd4004fdf0c8b25ba4f5156662f6244c5, 18, 'Masculino'),
(4, 'Mujer', 'e', 0xd4004fdf0c8b25ba4f5156662f6244c5, 18, 'Femenino'),
(5, 'usuario', 'usuario@correo.com', 0x2c1a8cdf21197e1fd07792fa9d786477, 18, 'Masculino');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `resultados`
--
ALTER TABLE `resultados`
  ADD PRIMARY KEY (`id_resultado`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `resultados`
--
ALTER TABLE `resultados`
  MODIFY `id_resultado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `resultados`
--
ALTER TABLE `resultados`
  ADD CONSTRAINT `resultados_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
