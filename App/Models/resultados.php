<?php
namespace modelos;
class Resultados extends Conexion{
    
    public function __construct(){
        parent::__construct();
    } //constructor de resultados
    static function fechas(){
               
        ?>
        <div class="insertados">
            <form action="index.php?controller=General&action=calculoResultados" method="POST">
                <label>Fecha de inicio</label>     
                <br><br>
                <input name="fechaInicio" type="date" value="2021-02-21" min="2021-02-21"> 
                <br><br>
                <label>Fecha Final</label>        
                <br><br>
                <input name="fechaFinal" type="date" value="2021-02-22" min="2021-02-22"> 
                <br><br>
                <input type="submit" value="Calcular">
            </form>
        </div>
        <?php
    }// formulario para calculo de hombres y mujeres por fecha
    static function idDiferente(){
        $conexion=new Conexion();
        $sql=$conexion->conect->query("SELECT count(id_usuario) as maximo FROM usuarios"); 
        $conteo=$sql->fetch_assoc();  
        $idDistinta=0;
        do{
            $idDistinta=rand (1,$conteo['maximo']);
            echo $idDistinta;
        }while($idDistinta==$_SESSION['id_usuario']);
        return $idDistinta;
    }//generar una id distinta a la del usuario conectado
    static function graficaBarras($idDistinta){
        $conexion=new Conexion();        
        $sql2=$conexion->conect->query
        ("SELECT 
        puntaje,usuarios.nombre 
        FROM resultados 
        INNER JOIN usuarios 
        ON usuarios.id_usuario=resultados.id_usuario 
        where 
        usuarios.id_usuario=$idDistinta or usuarios.id_usuario=".$_SESSION['id_usuario']." order by puntaje "); 
        $resultados=$sql2->fetch_assoc();  
        $valoresX=array();
        $valoresY=array();
        do {
            
            $valoresX[]=$resultados['nombre'];
            echo "X";
            print_r($valoresX);
            $valoresY[]=$resultados['puntaje'];
            echo "Y";
            print_r($valoresY);
        }while ($resultados=$sql2->fetch_assoc());
        $datosX=json_encode($valoresX);
        $datosY=json_encode($valoresY);
        ?>
      
        <div id="graficaBarras"></div>
        <script type="text/javascript">
            function crearCadenaBarras(json) {
                var parsed= JSON.parse(json);
                var arr=[];
                for(var x in parsed){
                    arr.push(parsed[x]);
                }
                return arr;
            }
        </script>
        
        <script type="text/javascript">
        
            datosX=crearCadenaBarras('<?php echo $datosX?>');
            datosY=crearCadenaBarras('<?php echo $datosY?>');
            var data = [
            {
                x: datosX,
                y: datosY,
                type: 'bar'
            }
            ];
        
            Plotly.newPlot('graficaBarras', data);
        </script> 
        <?php
        
    }//creacion de datos para grafica de barras (comparacion entre 2 usuarios)
    static function promedioMasculino($fechaInicio,$fechaFinal){
        $conexion=new Conexion();
        $prepararMasculinoSQL="CALL calculoPromedioMasculino('$fechaInicio','$fechaFinal');";
        $queryMasculino=$conexion->conect->query($prepararMasculinoSQL);
        $promedioMasculino=$queryMasculino->fetch_assoc();        
        print_r($promedioMasculino);
        return $promedioMasculino['promedioM'];
    }//obtencion del promedio general masculino
    static function promedioFemenino($fechaInicio,$fechaFinal){
        $conexion=new Conexion();
        $prepararFemeninoSQL="CALL calculoPromedioFemenino('$fechaInicio','$fechaFinal');";
        $queryFemenino=$conexion->conect->query($prepararFemeninoSQL);
        $promedioFemenino=$queryFemenino->fetch_assoc();        
        print_r($promedioFemenino);
        return $promedioFemenino['promedioF'];
    }//obtencion del promedio general femenino
    static function graficaPastel($promedioFemenino,$promedioMasculino){
        

       
        ?>
        <script type="text/javascript">
           
            var trace1 = {
                x: datosX,
                y: datosY,
                type: 'scatter'
            };

            
            var data = [trace1];
            Plotly.newPlot('graficaLineal', data);
        </script>
         <div id="myDiv"></div>
        <script type="text/javascript">
            var data = [{
            values: [<?php  echo $promedioMasculino; ?>, <?php echo $promedioFemenino; ?>],
            labels: ['Hombres','Mujeres'],
            type: 'pie'
            }];

            var layout = {
            height: 500,
            width: 500
            };

            Plotly.newPlot('myDiv', data, layout); 
            </script>
        <?php
    }//Grafica de pastel hombres vs mujeres
    static function impresionGrafica(){
        ?>
        <div class="container">
		    <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">
                            Estadisticas de los precios.
                        </div>
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-sm-6">

                                    <h3><center>10 Productos Más Baratos</center></h3>
                                    <div id="cargaLineal"></div>
                                </div>
                                <div class="col-sm-6">

                                    <h3><center>10 Productos Más Caros</center></h3>
                                    <div id="cargaBarras"></div>
                                </div>
                                

                                </div>

					    </div>
				    </div>
			    </div>
		    </div>
	    </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel panel-heading">
                            Productos y totales.
                        </div>
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-sm-6">

                                    <div id="cargaLineal2"></div>
                                </div>
                                <div class="col-sm-6">


                                    <div id="cargaBarras2"></div>
                                </div>
                                

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        /*    $(document).ready(function(){
                $('#cargaLineal').load('graficas/lineal.php');
                $('#cargaBarras').load('graficas/barras.php');
                $('#cargaLineal2').load('graficas/lineal2.php');
                $('#cargaBarras2').load('graficas/barras2.php');
            });*/
        </script>
        <?php
    }//impresion de las graficas requeridas
}

?>