<?php
namespace modelos;
class Principal extends Conexion{
public $id_usuario;
public $nombre;
public $correo;
public $contrasenia;
public $llave;
public $zona;
public $descripcion;
public function __construct(){
  parent::__construct();  
}
static function comprobardo($usuario,$contrasenia){
  $conexion=new Conexion();
  $preparar=mysqli_prepare($conexion->conect,"SELECT * FROM usuarios WHERE nombre=? AND 
  AES_DECRYPT(contrasenia,'llave')=?");
  $preparar->bind_param("ss",$usuario,$contrasenia);
  $preparar->execute();
  $siesta=  $preparar->get_result();
  return $siesta->fetch_assoc(); 
} //comprobar si existe usuario
static function test(){
 ?>
 	<script>
		function resultado() {
			var p1, p2, p3, p4,p5,p6,p7,p8,p9,p10, nota;

				//<!-- 1ª pregunta -->
			if (document.getElementById('p11').checked==true) {p1=1}
				else {p1=0}
				//<!-- 2ª pregunta -->
			if (document.getElementById('p22').checked==true) {p2=1}
			else {p2=0}
				//<!-- 3ª pregunta -->
			if (document.getElementById('p33').checked==true) {p3=1}
				else {p3=0}
				//<!-- 4ª pregunta -->
			if (document.getElementById('p44').checked==true) {p4=1}
				else {p4=0}
				//<!-- 5ª pregunta -->
			if (document.getElementById('p54').checked==true) {p5=1}
				else {p5=0}

				//<!-- 6ª pregunta -->
			if (document.getElementById('p64').checked==true) {p6=1}
				else {p6=0}
				//<!-- 7ª pregunta -->
			if (document.getElementById('p74').checked==true) {p7=1}
				else {p7=0}
				//<!-- 8ª pregunta -->
			if (document.getElementById('p84').checked==true) {p8=1}
				else {p8=0}
				//<!-- 9ª pregunta -->
			if (document.getElementById('p94').checked==true) {p9=1}
				else {p9=0}
				//<!-- 10ª pregunta -->
			if (document.getElementById('p104').checked==true) {p10=1}
				else {p10=0}
			nota=p1+p2+p3+p4+p5+p6+p7+p8+p9+p10;      
			alert(" Aciertos: "+nota);
			window.location.href = "index.php?controller=General&action=guardar" + "&guardar=" + nota;
	
		}
	</script>
 <form class="preguntas" style="font:normal 16px sans-serif">
		<div class="gallerycard">
			<p>1- Es la sintaxis, (forma de escribirse), de una consulta.</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta1" id="p11"> -select [campos] from [tabla] …[condiciones]</p>
			<p><input type="radio" name="pregunta1" id="p12"> select [tabla] from [campos] …[condiciones]</p>
			<p><input type="radio" name="pregunta1" id="p13"> select [database] from [tabla]</p>
			<p><input type="radio" name="pregunta1" id="p14"> select [condiciones] from [database] …[campos]</p><br>
		</div>


		<div class="gallerycard">
			<p>2.- De qué forma se declara una llave primaria</p>
			<p><input type="radio" name="pregunta2" id="p21">(Tipo dato) [Nombre campo] foreign key</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta2" id="p22">(Nombre campo) [tipo dato] primary key</p>
			<p><input type="radio" name="pregunta2" id="p23"> (Nombre campo) [tipo dato] foreign key	</p>
			<p><input type="radio" name="pregunta2" id="p24"> b) (Tipo dato) [Nombre campo] primary key</p><br></div>


		<div class="gallerycard">
			<p>3.- ¿Como se pueden ver todas las bases de datos?</p>
			<p><input type="radio" name="pregunta3" id="p31">a) show databases [nombre de la base]	</p>
			<p><input type="radio" name="pregunta3" id="p32">b) show [nombre de la base] databases</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta3" id="p33">--c) show databases</p>
			<p><input type="radio" name="pregunta3" id="p34"> d) show tables from databases</p><br></div>


		<div class="gallerycard">
			<p>4.- ¿Cómo se declara una llave primaria compuesta?</p>
			<p><input type="radio" name="pregunta4" id="p41">a) foreign key(campo1, campo2,…)		</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta4" id="p44">-- b) primary key(campo1, campo2, …);</p>
			<p><input type="radio" name="pregunta4" id="p42">c) create primary key[tabla,campos]</p>
			<p><input type="radio" name="pregunta4" id="p43">d) Alter table [tabla] add primary key[database]</p><br></div>

		<div class="gallerycard">
			<p>5.- ¿Cómo se agrega un campo a una tabla?</p>
			<p><input type="radio" name="pregunta5" id="p51"> a) alter table [tabla] add [tabla] [tipo dato]</p>
			<p><input type="radio" name="pregunta5" id="p52"> b) select [table] add [campo] [tipo dato]</p>
			<p><input type="radio" name="pregunta5" id="p53"> c) alter database [tabla] add [campo] after [tabla]</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta5" id="p54">--d) alter table [tabla] add [campo] [tipo de dato] [position] [campo]</p><br></div>

		<div class="gallerycard">
			<p>6.- ¿Cuál es la sintaxis para borrar el contenido de un campo especifico?</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta6" id="p64">--a) DELETE FROM [tabla] WHERE [campo] = [valor]</p>
			<p><input type="radio" name="pregunta6" id="p62">b) DELETE FROM [database] WHERE [table] = [valor]</p>
			<p><input type="radio" name="pregunta6" id="p63">c) DELETE [campo] FROM [table] WHERE [campo]=[valor]	</p>
			<p><input type="radio" name="pregunta6" id="p61">d) DELETE [valor] FROM [table] WHERE [valor]=[campo]</p><br></div>

		<div class="gallerycard">
			<p>7.- ¿Cómo se pueden borrar todos los registros de una tabla por completo?</p>
			<p><input type="radio" name="pregunta7" id="p71"> a) DELETE FROM [tabla] WHERE [condición]</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta7" id="p74"> --b) TRUNCATE TABLE [tabla] o DELETE FROM [tabla]</p>
			<p><input type="radio" name="pregunta7" id="p73"> c) TRUNCATE TABLE [tabla] WHERE [condición]</p>
			<p><input type="radio" name="pregunta7" id="p72"> d) DELETE all FROM [table]</p><br></div>

		<div class="gallerycard">
			<p>8.- ¿Cómo se actualiza un registro específico?</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta8" id="p84">--a) UPDATE [table] SET [campo]=[valor] WHERE [campo]=[valor]</p>
			<p><input type="radio" name="pregunta8" id="p82">b) UPDATE [table] SET [campo]=[valor] WHERE [table]=[valor]</p>
			<p><input type="radio" name="pregunta8" id="p83">c) UPDATE [table] SET [table]=[campo]</p>
			<p><input type="radio" name="pregunta8" id="p81"> d) UPDATE [table] SET WHERE [table]=[campo]</p><br></div>

		<div class="gallerycard">
			<p>9.¿Cómo se pueden actualizar todos los datos de una tabla?</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta9" id="p94">--a) UPDATE [table] SET [campo]=[valor]		</p>
			<p><input type="radio" name="pregunta9" id="p92"> b) UPDATE [table] SET [database]=[table]</p>
			<p><input type="radio" name="pregunta9" id="p93"> c) UPDATE [table] SET [table]</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta9" id="p91"> d) UPDATE [campo] SET [campo]=[valor]</p><br></div>

		<div class="gallerycard">
			<p>10.- ¿Cómo se puede especificar un limite en los valores devueltos por una consulta?</p>
			<p><input type="radio" name="pregunta10" id="p101">a) SELECT * FROM [tabla] TOP [valor].</p>
			<p onclick="this.style.color='#00FF00'"><input type="radio" name="pregunta10" id="p104">-- b) SELECT * FROM [tabla] LIMIT (valor1),(valor2)</p>
			<p><input type="radio" name="pregunta10" id="p103">c) SELECT * FROM [campo] LIMIT (campo1),(campo2)	</p>
			<p><input type="radio" name="pregunta10" id="p102">d) SELECT * FROM [campo] LIMIT (database1),(database2)</p><br></div>
			<hr>
			<p><input type="button" value=" R E S U L T A D O " onclick="resultado()"/></p>
	</form>
 	<?php       
}// impresion y realizacion de test
static function menu(){
  ?>
  <header class="inicio">
        <nav class="menu1">
            <!-- Menu y buscador-->

            
                <ul class="menu">                  
                    <li><a href="index.php?controller=General&action=todo" class="btn1">Comenzar Test</a></li>
                    <li><a href="index.php?controller=General&action=comparar" class="btn1">Comparar resultados</a></li>
                    <li><a href="index.php?controller=General&action=resultadosSemanales" class="btn1">Resultados Semanales</a></li>
                    <li><a href="index.php?controller=Principal&action=salir" class="btn1">Salir</a></li>
                </ul>
            
            <!--Logotipo-->

        </nav>
    </header>
    <?php
}//menu global
static function contarTest(){
	$conexion=new Conexion();
    $prepararSQL="Select count(puntaje) as veces from resultados where id_usuario=".$_SESSION['id_usuario'];
    $ejecutarQuery=$conexion->conect->query($prepararSQL);
    $resultado=$ejecutarQuery->fetch_assoc();
	echo "<h1>Has contestado este test ".$resultado['veces']." veces</h1>";
}//contador de veces respondidas al test
static function registro($usuario,$puntaje){
  $conexion=new Conexion();
  $preparar=mysqli_prepare($conexion->conect,
  "INSERT INTO resultados VALUES(NULL,?,?,CURRENT_DATE())");
  $preparar->bind_param("ss",$puntaje,$usuario);
  $preparar->execute();
  header('location:index.php?controller=General&action=comparar');
  
}//registro de puntaje de test
static function registroUsuarios($usuario,$correo,$contrasenia,$edad,$sexo){
	$conexion=new Conexion();
	$preparar=mysqli_prepare($conexion->conect,
	"INSERT INTO usuarios VALUES(NULL,?,?,AES_ENCRYPT(?,'llave'),?,?)");
	$preparar->bind_param("sssss",$usuario,$correo,$contrasenia,$edad,$sexo);
	$preparar->execute();
	$preparar->get_result();
	return $preparar;
	//header('location:index.php?controller=General&action=resultadosSemanales');
	
  }
}//registro de usuarios nuevos
?>