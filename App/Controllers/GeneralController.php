<?php
require 'App/Models/conexion.php';
require 'App/Models/principal.php';
require 'App/Models/resultados.php';
use conectar\Conexion;
use modelos\Principal;
use modelos\Resultados;

class GeneralController{
    public function __construct(){
        if($_GET["action"]=="todo"||$_GET["action"]=="resultadosSemanales"||$_GET["action"]=="comparar"){
            
            if(!isset($_SESSION['usuario'])){
                echo "no has iniciado sesion";
                header('Location:index.php?controller=Principal&action=iniciar');
            }
        }
    } //constructor de controlador general
    public function todo(){    
        require 'app/view/general.php';
        principal::menu();   
        principal::contarTest();          
        principal::test(); 
        
    }//opcion todo (contestar test)
    public function guardar(){
        require 'app/view/general.php';
        principal::menu();   
        $id_usuario=$_SESSION['id_usuario'];
        $puntaje=$_GET['guardar']; 
        principal::registro($id_usuario,$puntaje);
    }//guardar datos del test
    public function comparar(){    
        require 'app/view/general.php';
        principal::menu();   
        $idNueva=resultados::idDiferente();                      
        resultados::graficaBarras($idNueva); 
    }//opcion para ir a comparacion entre 2 usuarios
    public function resultadosSemanales(){
        require 'app/view/general.php';
        principal::menu();
        resultados::fechas();
    }//opcion para fechas grafica de pastel
    public function calculoResultados(){
        require 'app/view/general.php';
        principal::menu();
        resultados::fechas();
        if(isset($_POST['fechaInicio'])){
            $fechaInicial=$_REQUEST['fechaInicio'];
            $fechaFinal=$_REQUEST['fechaFinal'];
            $promedioFemenino=resultados::promedioFemenino($fechaInicial,$fechaFinal);
            $promedioMasculino=resultados::promedioMasculino($fechaInicial,$fechaFinal);
            resultados::graficaPastel($promedioFemenino,$promedioMasculino);   
        }      
        /*$opcion=$_GET['opcion'];
        $this->{$opcion}();*/
    }//opcion para imprimir grafica de pastel
 
}

?>