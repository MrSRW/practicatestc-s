<?php
require 'App/Models/conexion.php';
require 'App/Models/principal.php';
use conectar\Conexion;
use modelos\Principal;
class PrincipalController{
    public function __construct(){        
        if($_GET["action"]=="ingreso"){            
            if(!isset($_SESSION['usuario'])){
                echo "no has iniciado sesion";
                header('Location:index.php?controller=Principal&action=iniciar');
            }
        }
    }
    function iniciar(){//inicio de pagina (login) 
        require 'app/view/inicio.php';
    }
    function ingreso(){//inicio de pagina (Una vez logueado) 
        require 'app/view/general.php';
        
            if(isset($_SESSION['usuario'])){
                header('Location:index.php?controller=General&action=resultadosSemanales');
            }
        
        //header('Location:index.php?controller=General&action=menu');
    }
    public function ingresar(){//iniciar sesión    
        if (isset($_REQUEST['u']) && !empty($_REQUEST['u'])){
            $u=$_REQUEST['u'];
            $p=$_REQUEST['p'];
            
            $verificar=principal::comprobardo($u,$p);           
            if(!$verificar){
                require 'app/view/inicio.php';
                echo "<h1>Datos erroneos</h1>";
            }else{
                $_SESSION['usuario']=$verificar;
                $_SESSION['id_usuario']=$verificar['id_usuario'];
                header('Location:index.php?controller=Principal&action=ingreso');
            }            
        }
    }
    function registrar(){ //registra nuevo usuario
        $usuario=$_REQUEST['usuario'];
        $correo=$_REQUEST['correo'];
        $edad=$_REQUEST['edad'];
        $sexo=$_REQUEST['sexo'];
        $contrasenia=$_REQUEST['contrasenia']; 
        $contrasenia2=$_REQUEST['contrasenia1'];
        if($contrasenia==$contrasenia2){
            $registro=principal::registroUsuarios($usuario,$correo,$contrasenia,$edad,$sexo);
            print_r($registro);  
            require 'app/view/inicio.php';                
            echo "<h1>Datos registrados</h1>";            
        }else{
            require 'app/view/inicio.php';
            echo "<h1>Contraseñas no correctas</h1>";
            echo "<a href='index.php?controller=Principal&action=iniciar'>Volver</a>";
        }
        
//INSERT INTO administrador VALUES(null,'alejandroAdmin',AES_ENCRYPT('contraseña','llave'));
    }
    function salir(){
        unset($_SESSION['usuario']);
        unset($_SESSION['id_usuario']);
        header('Location:index.php?controller=Principal&action=iniciar');

    }
}

?>